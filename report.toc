\contentsline {section}{\numberline {1}Letter from the President}{3}{section.1}
\contentsline {section}{\numberline {2}Our Mission}{5}{section.2}
\contentsline {subsection}{\numberline {2.1}Modernizing Our Mission Statement}{5}{subsection.2.1}
\contentsline {section}{\numberline {3}What We Accomplished This Year}{6}{section.3}
\contentsline {section}{\numberline {4}Vision for 2021}{7}{section.4}
\contentsline {subsection}{\numberline {4.1}A Short Take on the Long Road to Now}{7}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}What Comes Next}{8}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Well That Sounds Promising}{9}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Your Role In This}{9}{subsection.4.4}
\contentsline {section}{\numberline {5}Words From Our Members}{11}{section.5}
\contentsline {section}{\numberline {6}Talks and Presentations}{12}{section.6}
\contentsline {section}{\numberline {7}Board Members}{16}{section.7}
\contentsline {section}{\numberline {8}Staff}{22}{section.8}
\contentsline {section}{\numberline {9}Sponsorship Tiers}{25}{section.9}
\contentsline {subsection}{\numberline {9.1}Anchor Sponsors}{25}{subsection.9.1}
\contentsline {subsection}{\numberline {9.2}Premium Sponsors}{26}{subsection.9.2}
