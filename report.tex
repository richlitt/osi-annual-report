\documentclass[landscape,12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,
    urlcolor=cyan,
}
\urlstyle{same}
\usepackage[export]{adjustbox}

\ifxetex
  \usepackage{letltxmacro}
  \setlength{\XeTeXLinkMargin}{1pt}
  \LetLtxMacro\SavedIncludeGraphics\includegraphics
  \def\includegraphics#1#{% #1 catches optional stuff (star/opt. arg.)
    \IncludeGraphicsAux{#1}%
  }%
  \newcommand*{\IncludeGraphicsAux}[2]{%
    \XeTeXLinkBox{%
      \SavedIncludeGraphics#1{#2}%
    }%
  }%
\fi

\input{defs}

\textheight=15.5cm
\footskip=1.5cm

%\setlength{\parskip}{1em}

%\definecolor{truewhite}{HTML}{FFFFFF}

\usepackage{lipsum,pagecolor}


%%%%%%%%%%%%%%%
% Title Page
\title{Open Source Initiative 2020 Annual Report}
%\author{Richard Littauer\\Sustainability Co\"ordinator}
%\date{\today}
\summary{
%An annual report for the Open Source Initiative, wherein we discuss many pertinent things.
}
%%%%%%%%%%%%%%%
% \linespread{1.5}
\begin{document}
\pagecolor{white!}
\maketitle

\tableofcontents
\clearpage

\section{Letter from the President}

\begin{wrapfigure}{R}{0.25\textwidth}
 \raisebox{-210pt}[\height]{%
         \includegraphics[width=0.06\textwidth]{photos/josh.jpg}}
\end{wrapfigure}

Hello friends,

I hope my message finds you in good health and good spirits. It’s difficult to find the words for this year’s annual letter. I write to you at the end of a devastating year, with hope for better times and a flickering possibility of transformation on the horizon. Every single one of us has been profoundly altered by the pandemic and second order crises. The pandemic has forced us all to find our North Star and muddle our way through waves of change. This is true of \href{https://opensource.org/civicrm/mailing/url?u=2368\&qid=305394}{individuals, and organizations} – OSI and the humans who power it included.

It is with great pride I say to you: even as we coped with the pandemic's fallout, the people of OSI and open source at large have been \href{https://opensource.org/civicrm/mailing/url?u=2369\&qid=305394}{hard at work} charting a course to a more robust Open Source Initiative and a more resilient open source ecosystem. We end the year with a lot to be proud of, and hopeful for. Much of that was on display at our inaugural \href{https://opensource.org/civicrm/mailing/url?u=2370\&qid=305394}{State of the Source Summit} back in September. You can \href{https://opensource.org/civicrm/mailing/url?u=2371\&qid=305394}{catch that on video} if you missed it!

One of the most visible changes for friends of OSI this year was the changing of the guard: after 7 years as OSI’s steady hand, General Manager Patrick Masson resigned and noted open source advocate \href{https://opensource.org/civicrm/mailing/url?u=2372&qid=305394}{Deb Nicholson was hired} as Interim General Manager. Patrick was OSI’s first full time employee and lead our growth over seven years, exiting with a mutual understanding that OSI’s needs are evolving with a call for new leadership.  We are especially grateful for Patrick driving his thoughtful stewardship during his tenure here.

Everything we accomplished this year was a team effort. We couldn’t have done it without you and the communities we serve. Beyond the meta work of improving OSI and cultivating a healthier ecosystem in the face of disaster, we saw explosive growth in the adoption of open source software and increased reliance on all the dependencies incorporated into our digital infrastructure driven by the need for billions of people to remain isolated at home.

Increased dependence on digital infrastructure makes open source all the more vital – we need infrastructure powered by software we can audit, trust, and improve rapidly through collaboration. Looking back, I’m inspired by how we came together this year. I’m excited about where open source is going and the things we can do to continue collaborating and help people share and connect. OSI spent much of 2020 driving \href{https://opensource.org/civicrm/mailing/url?u=2373&qid=305394}{strategic planning} and we are extremely pleased to say we're set up for a big growth year in 2021!

First, we’re seeking to hire an \href{https://opensource.org/civicrm/mailing/url?u=2374&qid=305394}{hire an Executive Director}. We’re accepting applications now and we’re pretty sure that, if you’re reading this letter, you’re either a good candidate or know someone who would be. With that in mind: please, help us get the word out and share the job posting! Perhaps more importantly, expect many more remote conversations about open source. If nothing else, the last few years have taught us that OSI needs to use its unique position to convene conversations proactively – so that’s what we’re going to do. This is an organization of, by, and for the community after all! We hope you’ll join us for those over the course of 2021 and the coming years.

We couldn’t do all these amazing things to support the open source community without you! We hope to see you contributing to open source alongside us -- whether your gift is time, money, encouragement or even thoughtful criticism. Please get in touch if you want to jump in but would like suggestions on where to start.

\bigskip
\begin{flushright}
In service,

\bigskip

Josh Simmons

President

Open Source Initiative

\end{flushright}
\newpage
\section{Our Mission}


\fullboxbegin
\begin{center}
{\Large As steward of the OSD, we set the foundation for the open source software ecosystem.}
\end{center}
\fullboxend


\subsection{Modernizing Our Mission Statement}

Our old mission statement didn’t succinctly encompass all the work that we do at OSI, as the organization finds itself in a very different place than when we started in 1998. The needs of the open source software ecosystem are far more complicated and globally connected nowadays, and we aim to meet these needs by embracing change and updating our tactics.

Our new mission is: ``\textit{As steward of the OSD, we set the foundation for the open source software ecosystem.}'' This replaces the old mission statement, ``\textit{We are the stewards of the Open Source Definition (OSD) and the community-recognized body for reviewing and approving licenses as OSD-conformant.}'' Our positions have not changed, but the activities that we focus on going forward will continue to extend beyond license approval. We remain stewards of the Open Source Definition (aka OSD) but we will also be looking for other ways to support, grow and maintain the open source ecosystem.

This change is part of a year-long strategic planning process where we have been working toward what we can accomplish and sustain in the future. With this new, tidier mission statement as a rudder, we’ll be exploring new tactics, modernizing our outreach, and assessing what processes and infrastructure we’ll need to accelerate the next 20 years of open source. We hope our new, clearer mission statement resonates with you, and we welcome your voice and support in the work to make it a reality.

\newpage
\section{What We Accomplished This Year}

2020 was an unusual year for the OSI, as for everyone. During the pandemic, we were able to take space to reevaluate our strategies in the context of our overarching missions. While working on our ongoing remote activities like the micro-courses and the license review, we were also able to set a course 2021 and beyond. As well, this year we:

\begin{itemize}
\item Approved four new licenses through our license review process
\item Organized ``\href{https://opensource.org/StateOfTheSource}{State of the Source}" a global open source event online
\item Started planning an overhaul of our license review process
\item Developed \href{https://opensource.org/node/1058}{six new ``micro-courses"} with Brandeis University
\item Filed an \href{https://opensource.org/node/1034}{Amicus Brief} in the critical Google vs. Oracle court case
\item Released a \href{https://opensource.org/node/1093}{Report on OSI Members and Stakeholders}
\item Supported open source friendly gains in government policy
\item Initiated an internal \href{https://opensource.org/node/1068}{strategic planning process} to sharpen our vision
\item Welcomed in four new \href{https://opensource.org/affiliates}{Affiliate Organizations}; OpenUK, the .NET Foundation, OASIS, and the OpenJS Foundation
\end{itemize}

And we also managed a big staff transition, bidding Patrick Masson goodbye and \href{https://opensource.org/node/1080}{welcoming Deb Nicholson} on as our Interim General Manager and then beginning \href{https://opensource.org/executive_director}{our Executive Director search}. We are looking forward to hiring our new ED in mid-2021, and potentially adding new staff in 2022, continuing our voyage to new horizons.


\newpage

\section{Vision for 2021}

% Lifted from https://opensource.org/node/1068

This is an interesting time for open source.

An approach to intellectual property that was once seen as radical is
now mainstream. In 2011, 13 years after ``open source'' was coined and
the Open Source Initiative was founded to promote and protect it,
O'Reilly Media declared that
\href{https://opensource.com/life/11/7/oscon-2011-disruption-default}{open
source had won}. In 2016,
\href{https://www.wired.com/2016/08/open-source-won-now/}{WIRED}
followed suit. Now, open source undergirds software development across a
truly unfathomable range of applications and fans the flames of other
open culture movements. It has inspired new ways of collaborating with
each other, experiments in community governance, and has been so
successful that it is colloquially taken to mean all of the above.

And yet, open source \emph{feels} so tenuous sometimes. Questions dog
us. Setting aside run-of-the-mill fear, uncertainty, and doubt, people
are raising legitimate questions: are our projects sustainable? Are our
communities safe and healthy? Are maintainers being treated fairly? Is
our work just? Can open source weather continued attempts at
redefinition?

These concerns are not new, but the scale they're playing out on is. And
Open Source Initiative--though it has sustained its core mission around
licensing for 22 years, slogging through the legal janitorial work that
makes open source adoption easy--has simply not been a leading voice in
these other conversations.

Even on the topic of licensing, OSI has been found on its back foot. Our
response to the recent flare ups of open core and source available
licensing was lackluster. Everyone agrees: open source needs a bolder,
more responsive, and representative OSI.

How do we get there? We have a plan, and you're part of it.

\subsection{A Short Take on the Long Road to Now}

\begin{wrapfigure}{L}{0.40\textwidth}
\includegraphics[width=0.4\textwidth]{photos/brandeis.png}
\caption*{A normal day during a Brandeis class}
\end{wrapfigure}

The key to understanding how we move forward is to first remember how we
got here. OSI as we know it didn't exist until 2013.~

Founded in 1998, the organization was held together in its first decade
through strong board leadership in Michael Tiemann (2001-2012) and
Danese Cooper (2002-2011). Deb Bryant (2012-present), Karl Fogel
(2011-2014), Mike Milinkovich (2012-2018), and Simon Phipps (2010-2020)
helped OSI begin professionalizing, by hiring General Manager Patrick
Masson (2013-2020), and becoming more democratic, with the
introduction of a community-elected board. Molly de Blanc (2016-2020),
Allison Randal (2014-2019), and Stefano ``Zack'' Zacchiroli (2014-2017)
fostered better ties with the free software community. Richard Fontana
(2013-2019) elevated legal discussions, taking OSI's licensing work from
knowledgeable hackers to expert practitioners and defining a review
process. And Pam Chestek (2019-present) has brought a new level of
professionalism to the license review process.

This is a reductionist and inevitably incomplete view of OSI's history,
but the point is this: OSI has come a long way, and I am forever
grateful to the talented and generous individuals who collectively
invested decades to get us here.


Over the last seven years, OSI has: sustained its core mission, shaped
policy around the globe, worked tirelessly to mitigate open washing,
built an alliance of more than 125 organizations representing hundreds
of thousands of people, provided a home for projects like
\href{https://clearlydefined.io/}{ClearlyDefined}, and rolled out
programs like
\href{https://opensource.com/article/17/9/floss-desktops-kids}{FLOSS
Desktops for Kids} and
\href{https://www.brandeis.edu/gps/future-students/learn-about-our-programs/open-source-technology-management.html}{Open
Source Technology Management} courses with Brandeis University.

We have seen incremental progress every year. OSI has expanded its
programs and refined its operations. The trouble is, our operational
capacity has not kept pace with the growing responsibility.

\subsection{What Comes Next}

Two years ago, the
\href{https://opensource.org/docs/board-annotated}{OSI Board} recognized
the need for another transformation, and made staffing the organization
a priority. Pursuing that has required us to shave a great number of
yaks! Along the way, we've identified several other key changes, all of
which are reflected in our
\href{https://wiki.opensource.org/bin/Operations/Annual+Planning+Process/2020-Annual-Plan}{Annual
Initiatives} and the efforts our
\href{https://opensource.org/organization}{Committees} have in flight.

In broad strokes, we have deep organizational development work to do
(more on that later) as well as community engagement initiatives
including:

\begin{itemize}
\itemsep1pt\parskip0pt\parsep0pt
\item
  Create more opportunities for people to make their voices heard and
  get involved with the process, by convening Working Groups and
  Advisory Boards to work in concert with our Committees.
\item
  Develop a communications plan and capabilities in order to be
  responsive to community developments, as well as lead and facilitate
  emerging conversations.
\item
  Invest in an updated Code of Conduct and moderation tools.
\item
  Continue investing in documentation in service of transparency.
\item
  Continue targeted recruitment in service of representation.
\item
  Hire people to serve as Community and Communications Manager
  supporting this work.
\end{itemize}

Suffice it to say, we have our work cut out for us!

\subsection{Well That Sounds Promising}

The good news is that OSI has never been in a better position than it is
now. We have all the right players on all the right bases. We're more
organized than ever. And an intense period in the spotlight has brought
the work we need to do into sharp focus.


The bad news is that we only have 5 months operating budget in reserve,
thus lack the funding to hire additional staff\ldots{} And we're likely
to suffer a 20-40\% drop in revenue due to the pandemic and resulting
economic downturn.

\subsection{Your Role In This}

This is where you come in. We need your voice to make sure we plot a
path forward that meets your needs, and we need your support to fund the
work ahead.


We'll be sharing about new opportunities to make your voice heard as
this work unfolds, but you can always reach us on
\href{https://opensource.org/resources}{social media},
\href{https://opensource.org/resources}{IRC}, and directly via our
\href{https://opensource.org/lists}{mailing lists} or
\href{https://opensource.org/contact}{contact form}. And the OSI Board,
being community-elected, is chock full of people whose job it is to
serve you--you can always reach out to us individually.

If you are not yet an \href{https://opensource.org/join}{Individual
Member}, we ask you to become one, which will keep you apprised of our
work and allow you to vote in our annual elections, or even nominate
yourself.

If you lead a nonprofit organization or community that believes in open
source, we encourage you to become an
\href{https://opensource.org/affiliates}{Affiliate Member}, which helps
us provide mutual support and allows you to nominate and vote in
elections.

And if your company relies on open source to conduct business, we ask
that you invest back into the community that makes your work possible by
becoming a \href{https://opensource.org/sponsors}{Corporate Sponsor}.

We're looking forward to collaborating with you. Together, we’ll make sure that open source continues to thrive.

\section{Words From Our Members}


\begin{center}
\includegraphics[width=.95\textwidth]{photos/quotes.png}
\end{center}

%

%\section{Quotes from people about why they support OSI}
%\section{Quotes from board members}
%\section{Quotes from Affiliates?}

\newpage
\section{Talks and Presentations}

The OSI didn't stop having presentations during the year; like the rest of the tech world, the presentations merely went virtual. Last September, we hosted the \href{https://opensource.org/StateOfTheSource}{State of the Summit}. Our goals were to:

\begin{itemize}
\item Share the current state of open source licenses: understanding their value and impediments to further adoption.
\item Identify current, non-technical, issues affecting open source software, development, and communities through the lenses of developers, companies, and projects.
\item Conceptualize and plan for what the future may hold for open source software as a community and the Open Source Initiative as an organization.
\end{itemize}

While there were many, many talks - most of them available online \href{https://eventyay.com/e/8fa7fd14/schedule}{here}, there were a few we want to highlight here: \\

	\href{https://www.youtube.com/watch?v=8T_w4bPPbC4}{
         \includegraphics[width=.3\textwidth]{photos/keynote.png}
         } \\

Josh Simmon's gave a keynote: \href{https://www.youtube.com/watch?v=8T_w4bPPbC4}{An Open Source Success at Sea}. \\

	\href{https://www.youtube.com/watch?v=AvVxBnxvvh4}{
         \includegraphics[width=.3\textwidth]{photos/osileadership.png}
         } \\

And most of the OSI board members joined a panel about Leadership: \href{https://www.youtube.com/watch?v=AvVxBnxvvh4}{OSI Leadership in Stage}.  \\

	\href{https://www.youtube.com/watch?v=sAVe9GKwbzk}{
         \includegraphics[width=.3\textwidth]{photos/guadeckeynote.png}
         } \\

Board members also presented at other conferences, such as FOSDEM, ATO, and more. Here is our President, Josh Simmons, giving the keynote - \href{https://www.youtube.com/watch?v=sAVe9GKwbzk}{Necessary, but Not Sufficient} - at \href{https://events.gnome.org/event/1/}{GUADEC}.

\newpage
\section*{Clearly Defined}

\begin{center}
         \includegraphics{photos/clearlydefined.png}
         \vspace{-10mm}
\end{center}

Clearly Defined has moved forward significantly in 2020, in that we hired a new, full-time staffer to work on it! Nell Shamrell-Harrington started in October. Their focus initially was:

\begin{itemize}
\item Redoing and automating the local dev environment so that it could be run completely locally and not require access to the ClearlyDefined Azure account (something that was a barrier to contributors from outside of Microsoft)
\item Triaging all pull requests across all the major ClearlyDefined development repos (all have now either been merged or closed with an empathetic explanation of why)
\item Triaging >100 issues across those repos. Currently, every issue has been labeled and all open bugs (with the exception of 2-3 I am waiting for more information on) are ones I have personally been able to reproduce
\item For any feature request issues, we are going through them 5 at a time at our biweekly community meetings to decide 1) Is this right for the project and 2) Is this something we could feasibly get to in the next year or two? If the answer to those questions is yes, the feature request remains open (and will be prioritized along with our ongoing work), if not, the feature request is closed with an empathetic explanation of why
\end{itemize}

Moving forward, our focuses for this year are:

\begin{itemize}
\item Add support for harvesting/defining Go packages
\item Add the ability for orgs to run a mirror of ClearlyDefined on their own infrastructure
\item Revamp the ClearlyDefined UI with a focus on usability and accessibility - we will be using our OSI funds to hire contractors for this
\end{itemize}

Clearly Defined saw more usage in 2020: the current stats have reached 10,850,000 packages harvested, with definitions across 11 package managers. When we add Go (and other language) components support this year, that number should grow significantly.

Additionally, when someone requests a definition from ClearlyDefined now, 97\% of the time that definition is already harvested, computed, and defined for them, they don't need to wait for the component to be harvested before seeing the definition.

\vspace{10mm}

You can stay up to date at \href{https://clearlydefined.io/}{https://clearlydefined.io/}.



\newpage
\section{Board Members}

\subsection*{Joshua R. Simmons, President}

\begin{wrapfigure}{L}{0.20\textwidth}
 \raisebox{-140pt}[\height]{%
         \includegraphics[width=0.045\textwidth]{photos/josh.jpg}}
\end{wrapfigure}

Josh Simmons is a web developer, armchair philosopher, and open source strategist starting his fifth year on the OSI board. He's been a freelancer and startup CEO (RIP) and has worked as a volunteer and professional community organizer. Josh currently does ecosystem strategy for Tidelift and serves as President of the Open Source Initiative.

Josh is recognized globally as an advocate for free and open source software, inclusive community building, and ethical technology. He works on multiple boards and is frequently sought out to advise and consult. Through this work, he builds bridges into software freedom and corporate open source, across cultural divides, and convenes all manner of stakeholders around shared interests.

In the four years Josh has been on the OSI board, he has served as CFO, VP, and Chair of the Staffing Committee. He's deepened relationships with FLOSS organizations around the world, represented OSI's views at summits and meetings, and has worked to build consensus in even the most fractious of times.

Josh has always endeavored to make the OSI a more bold, resilient, responsive, and representative organization. With renowned experts in licensing, standards, communications, and organizational development on the board, Josh's role is to facilitate in dogged pursuit of our mission.

\newpage


\begin{wrapfigure}{R}{0.22\textwidth}
\hspace{4mm}
\raisebox{0pt}[\height]{\includegraphics[width=0.2\textwidth]{photos/board/hong.jpg}}

\end{wrapfigure}


\subsection*{Hong Phuc Dang, Vice-President}

Hong Phuc Dang founded FOSSASIA in 2009 as a community devoted to improving people’s lives through sharing open technologies, knowledge and fostering global connections. Over a decade, she steers the organization, directs project teams, runs events and education programs, encourages open source participation across Asia. Hong also acts as an InnerSource manager at Zalando where she helps to bring open source best practices within the corporate walls and supports the company to become a good open source citizen.

\vspace{1.5in}

\begin{wrapfigure}{L}{0.17\textwidth}
 \raisebox{-12pt}[\height]{
    \includegraphics[width=0.15\textwidth]{photos/board/tracy.jpg}}
\end{wrapfigure}

\hfill
\hfill

\subsection*{Tracy Hinds, Treasurer / CFO}

Tracy Hinds is a director and CFO at Open Source Initiative. She spends her time being equally inspired by intention and ethics in building businesses and the conundrum surrounding the intersecting value in communities and business within the open source software ecosystem. Her prior work as a web engineer, community builder, OSS advocate, strategist and non-profit builder has led her through journeys at Samsung NEXT, Node.js/OpenJS Foundation, IBM Watson, Airship prior. She is also President of GatherScript, a non-profit cultivating inclusive web tech conferences.

\clearpage


\newpage


\subsection*{Deb Bryant, Secretary}

\begin{wrapfigure}{L}{0.22\textwidth}
 \raisebox{-102pt}[\height]{%
         \includegraphics[width=0.06\textwidth]{photos/board/bryant.jpg}}
          \hspace{5mm}
\end{wrapfigure}

Deb Bryant returns to the OSI board after several years away, following a six year term working with the board through governance reform and moving to a membership-driven organization.  By day she holds the position of Senior Director, Open Source Program Office, Office of the CTO at Red Hat where she leads a global team responsible for the company’s stewardship in open source software communities.

Deb draws her perspective from an industry-diverse background; Parallel and high-speed computing and commercialized internet and web applications in the 80s, commercial wide area networks, advanced telecommunications and data/voice convergence in the 90s, and worked squarely in the middle of the industry disruption created by open source software thereafter.  Her involvement in open source began in the 90s in Portland, Oregon where she became deeply involved in the community, helped grow OSUOSL into an international community resource, founded the Government Open Source Conference, and championed the use of FOSS in the public sector. Deb lent her voice to supporting open source project and developers, building bridges between academia, industry, and government along the way.

Deb serves on numerous boards where open source is a critical element of their mission in the pubic interest.  She serves on the advisory boards of Open Source Elections Technology Foundation, DemocracyLab, and the OASIS Open Project.  She also serves as an advisor to the Brandeis University / Open Source Initiative partnership for the new Open Technology Management program launching January 2020.

In 2010 Deb received the \href{http://en.wikipedia.org/wiki/O\%27Reilly_Open_Source_Award}{Open Source Award} in recognition of her contribution to open source communities and for her pioneering advocacy of open standards and the use of open source software in the public sector

\newpage


\subsection*{Chris Lamb, Asst. Secretary}

\begin{wrapfigure}{R}{0.24\textwidth}
\hspace{3mm}
 \raisebox{35pt}[\height]{%
         \includegraphics[width=0.23\textwidth]{photos/board/chris.jpg}}
\vspace{-20mm}
\end{wrapfigure}

Chris Lamb is a former Debian Project Leader, a life-long GNU/Linux user, the primary author of dozens of free/open-source projects and contributor to 100s of others. After working in the London tech scene, Chris became a freelancer and has been relishing the opportunities this has offered him with respect to contributing more to the open source community. Today, he is highly active in cross-distribution Reproducible Builds effort.

\vspace{2in}

\begin{minipage}{.18\textwidth}
\begin{flushleft}
\hspace{-6mm}
         \includegraphics[width=\textwidth]{photos/board/megan.jpg}
\end{flushleft}
\end{minipage}
\hfill
\begin{minipage}{.8\textwidth}
\subsection*{Megan Byrd-Sanicki, Asst. Treasurer}

Megan Byrd-Sanicki is the Manager, Research \& Operations for Google Open Source Program Office. With a decade of experience stewarding and advising open source projects and communities, Megan champions open source citizenship and sustainability within Google and the industry at large.
\end{minipage}

\newpage


\subsection*{Pamela Chestek}

\begin{wrapfigure}{R}{0.17\textwidth}
\hspace{4mm}
 \raisebox{45pt}[\height]{%
         \includegraphics[width=0.13\textwidth]{photos/board/pamela.jpg}}
         \vspace{-20mm}
\end{wrapfigure}

Pamela S. Chestek is the principal of Chestek Legal in Raleigh, North Carolina. She counsels creative communities on open source software, brand, marketing and copyright matters. Prior to returning to private practice, she held in-house positions at footwear, apparel, and high technology companies and was an adjunct law professor teaching a course on trademark law and unfair competition. She is a frequent author of scholarly articles, and her blog, Property, Intangible, provides analysis of current intellectual property case law. Pam has a Bachelor of Fine Arts from Penn State and a Juris Doctor from the Western New England University School of Law. She is admitted to practice in Connecticut, the District of Columbia, Massachusetts, New York and North Carolina, and has been certified by the North Carolina Board of Legal Specialization in Trademark Law.

\vspace{.75in}

\subsection*{Elana Hashman}

\begin{wrapfigure}{L}{0.23\textwidth}
 \raisebox{16pt}[\height]{%
         \includegraphics[width=0.21\textwidth]{photos/board/elana.jpg}}
         \vspace{-20mm}
\end{wrapfigure}

Elana Hashman is an open source software developer and free software advocate. Contributing to the Python, Debian, Clojure, JavaScript, and Kubernetes communities, she seeks to advance both the ideals and implementation of free and open source software. Elana is a Python Software Foundation Fellow and serves as a member of the Debian Technical Committee. She currently works for Red Hat as a Principal Site Reliability Engineer on the Azure Red Hat OpenShift team.

\newpage

\subsection*{Faidon Liambotis}

\begin{wrapfigure}{R}{0.25\textwidth}
\hspace{5mm}
 \raisebox{40pt}[\height]{%
         \includegraphics[width=0.23\textwidth]{photos/board/faidon.jpg}}
         \vspace{-30mm}
\end{wrapfigure}

Faidon Liambotis is a systems engineer, software developer, and free software hacker. He has been using, advocating for and contributing to free/open source software since the early 2000s. He has been a volunteer contributor and member of the Debian project in various capacities since 2006. Since 2012, he has been working for the Wikimedia Foundation, the non-profit organization that supports Wikipedia and the other Wikimedia free knowledge projects, most recently as a Director of Site Reliability Engineering.

\vspace{.6in}

\subsection*{Italo Vignoli}

\begin{wrapfigure}{L}{0.23\textwidth}
 \raisebox{17pt}[\height]{%
         \includegraphics[width=0.21\textwidth]{photos/board/italo.jpg}}
 \vspace{-22mm}
\end{wrapfigure}

Italo Vignoli has been involved in FOSS projects since 2004, when he joined the OpenOffice community as a user, to contribute to marketing and communication activities. In 2010, he was one of the founders of the LibreOffice project and has been involved in marketing and community development activities since then. He has also launched Associazione LibreItalia, representing LibreOffice users in Italy, and the ODF Advocacy Open Project at OASIS, and has contributed to large migration projects to LibreOffice in Europe.  He co-leads LibreOffice marketing, PR and media relations, co-chairs the certification program, and is a spokesman for the project. He has contributed to large migration projects to LibreOffice in Italy, and is a LibreOffice certified migrator and trainer. Italo is Managing Partner of Hideas, a marketing and communications agency retained by The Document Foundation and by other companies active in the networking and healthcare industries.

\newpage\section{Staff}

\subsection*{Deb Nicholson, General Manager}

\begin{wrapfigure}{L}{0.3\textwidth}
 \raisebox{15pt}[\height]{
 \includegraphics[width=.28\textwidth]{photos/deb.jpg}}
 \vspace{-10mm}
\end{wrapfigure}

Deb Nicholson, black background, looking upDeb Nicholson is a free software policy expert and a passionate community advocate. After years of local organizing on free speech, marriage equality, government transparency and access to the political process, she joined the free software movement in 2006. While working for the Free Software Foundation, she created the Women's Caucus to increase recruitment and retention of women in the free software community. She piloted messaging and directed outreach activities at the Open Invention Network, a shared defensive patent pool for free and open source software. She won the O’Reilly \href{http://en.wikipedia.org/wiki/O\%27Reilly_Open_Source_Award}{Open Source Award} for her work as GNU MediaGoblin's Community Liaison and as a founding board member at OpenHatch. She also continues to serve as a founding organizer of the Seattle GNU/Linux Conference, an annual event dedicated to surfacing new voices and welcoming new people to the free software community.

She received her Bachelor of Fine Arts from Bradford College and lives with her husband and her lucky black cat in Cambridge, Massachusetts.

\newpage


\subsection*{Jessica Iavarone, Operations Assistant}
\begin{minipage}{.8\textwidth}
\begin{flushleft}
\vspace{-20mm}
Jessica Iavarone is new to the Open Source team. She has recently joined on as Operations Assistant. Jessica graduated in 2017 from the University at Albany with a BA in Communications and Marketing. Jessica is also the Social Media Coordinator for the Northeast Kidney Foundation.
\end{flushleft}
\end{minipage}
\hfill
\begin{minipage}{.2\textwidth}
\begin{flushright}
\raisebox{10mm}[\height]{
         \includegraphics[width=\textwidth]{photos/jessica.png}}
\end{flushright}
\end{minipage}


\vspace{1in}

\subsection*{Phyllis Dobbs, Controller}

\begin{wrapfigure}{l}{0.2\textwidth}
 \raisebox{20pt}[\height]{%
         \includegraphics[width=0.2\textwidth]{photos/phyllis.png}}
         \vspace{-15mm}
\end{wrapfigure}

Phyllis Dobbs joined OSI as part-time Controller in December 2017. Phyllis is currently part-time Controller for Python Software Foundation, as well as a part-time accounting/finance consultant for the Lake Forest Symphony, the Lake County Chamber of Commerce, and the Alliance for Human Services. Previously, Phyllis was CFO of Shimer College and Controller for McGraw-Hill Higher Education. Phyllis has an MBA from Duke University and a B.S. in Accounting from Clemson University. In addition, Phyllis is Trustee at Cook Memorial Public Library, President of a condo association, volunteers annually with the IRS/AARP tax program, and is a clarinetist in the Libertvyille Village Band.

\clearpage

\begin{wrapfigure}{r}{0.2\textwidth}
\includegraphics[width=0.2\textwidth]{photos/richard.png}
\end{wrapfigure}

\subsection*{Richard Littauer, Sustainability Coordinator}

Richard Littauer is a free and open source software enthusiast. He has been working as a community manager and developer experience consultant for the past five years as part of his company, Maintainer Mountaineer. He helps facilitate the community and working groups at Sustain, a group dedicated towards figuring out how to keep FLOSS software going, and he runs a weekly podcast there. He also facilitates the OSPO++ community of open source program offices at universities and governments, outside of enterprise. Before diving full into open source, he was a full stack developer for MIT Media Lab and various startups, as well as an academic linguist. His graduate degrees in Linguistics were from Saarland University and the University of Edinburgh. In his spare time, he is an avid birder all over his adopted home state of Vermont.
\hfill
\vspace{1in}
\begin{wrapfigure}{l}{0.15\textwidth}
 \raisebox{20pt}[\height]{%
         \includegraphics[width=0.15\textwidth]{photos/simon.png}}
\end{wrapfigure}
\subsection*{Simon Phipps, Standards \& Policy Director}

Simon Phipps with his hands claspedSimon Phipps first joined OSI in 2008 as a Board observer and has been a board director, board president, and board secretary at various times since, until early 2020 when he switched to his current role. With a degree in electronic engineering that led to a focus first on compiler design and then workstations and networking, he has had C-level roles with responsibility for software community matters at IBM, Sun Microsystems and Forgerock. As Sun's chief open source officer he ran one of the first fully staffed OSPOs and oversaw the release of Sun's whole software portfolio under open source licenses, notably including the Java platform. He has been involved in de jure standards since 1991 at multiple SDOs.  He consults, writes and speaks widely on software freedom issues.


\newpage
\clearpage

\section{Sponsorship Tiers}

Our work at the OSI would not be possible without the support of all of our amazing sponsors.

\subsection{Anchor Sponsors}

\begin{figure}[!h]
\centering
\includegraphics[width=.6\textwidth]{photos/sponsors/google.png}
\end{figure}

\newpage
\subsection{Premium Sponsors}

\begin{figure}[!h]
\centering
\includegraphics[width=.2\textwidth]{photos/sponsors/IBM.png}\hspace{10mm}
\includegraphics[width=.2\textwidth]{photos/sponsors/comcast.png}\hspace{10mm}
\raisebox{20pt}[\height]{
\includegraphics[width=.2\textwidth]{photos/sponsors/digitalocean.png}\hspace{10mm}}
\raisebox{10pt}[\height]{
\includegraphics[width=.2\textwidth]{photos/sponsors/facebook.png}}
\includegraphics[width=.2\textwidth]{photos/sponsors/indeed.png}\hspace{10mm}
\includegraphics[width=.2\textwidth]{photos/sponsors/microsoft.png}\hspace{10mm}
\raisebox{-20pt}[\height]{
\includegraphics[width=.2\textwidth]{photos/sponsors/redhat.png}\hspace{10mm}}
\raisebox{-10pt}[\height]{
\includegraphics[width=.2\textwidth]{photos/sponsors/twitter.png}}
\end{figure}

\section*{Addendum}

This document was prepared by Richard Littauer and edited by Deb Nicholson. In so far as it was possible or reasonable, open source tools were used to prepare this report: it was written using LaTeX; compiled in TeXShop; images were edited using GIMP; file versioning was done using Git through GitLab. The image of Brandeis students was taken with their full consent for the purposes of including in this report.

The content on this document, of which Opensource.org is the author, is licensed under a Creative Commons Attribution 4.0 International License.

\end{document}
