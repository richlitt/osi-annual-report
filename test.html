
<p>This is an interesting time for open source.</p>

<p>An approach to intellectual property that was once seen as radical is now mainstream. In 2011, 13 years after "open source" was coined and the Open Source Initiative was founded to promote and protect it, O'Reilly Media declared that <a href="https://opensource.com/life/11/7/oscon-2011-disruption-default">open source had won</a>. In 2016, <a href="https://www.wired.com/2016/08/open-source-won-now/">WIRED</a> followed suit. Now, open source undergirds software development across a truly unfathomable range of applications and fans the flames of other open culture movements. It has inspired new ways of collaborating with each other, experiments in community governance, and has been so successful that it is colloquially taken to mean all of the above.</p>

<p>And yet, open source <em>feels</em> so tenuous sometimes. Questions dog us. Setting aside run-of-the-mill fear, uncertainty, and doubt, people are raising legitimate questions: are our projects sustainable? Are our communities safe and healthy? Are maintainers being treated fairly? Is our work just? Can open source weather continued attempts at redefinition?</p>
<!--break-->

<p>These concerns are not new, but the scale they're playing out on is. And Open Source Initiative--though it has sustained its core mission around licensing for 22 years, slogging through the legal janitorial work that makes open source adoption easy--has simply not been a leading voice in these other conversations.</p>

<p>Even on the topic of licensing, OSI has been found on its back foot. Our response to the recent flare ups of open core and source available licensing was lackluster. Everyone agrees: open source needs a bolder, more responsive, and representative OSI.</p>

<p>How do we get there? We have a plan, and you're part of it.</p>

<h2>A Short Take on the Long Road to Now</h2>

<p>The key to understanding how we move forward is to first remember how we got here. OSI as we know it didn't exist until 2013.&nbsp;</p>

<p>Founded in 1998, the organization was held together in its first decade through strong board leadership in Michael Tiemann (2001-2012) and Danese Cooper (2002-2011). Deb Bryant (2012-present), Karl Fogel (2011-2014), Mike Milinkovich (2012-2018), and Simon Phipps (2010-2020) helped OSI begin professionalizing, by hiring General Manager Patrick Masson (2013-present), and becoming more democratic, with the introduction of a community-elected board. Molly de Blanc (2016-2020), Allison Randal (2014-2019), and Stefano “Zack” Zacchiroli (2014-2017) fostered better ties with the free software community. Richard Fontana (2013-2019) elevated legal discussions, taking OSI’s licensing work from knowledgeable hackers to expert practitioners and defining a review process. And Pam Chestek (2019-present) has brought a new level of professionalism to the license review process.</p>

<p>This is a reductionist and inevitably incomplete view of OSI’s history, but the point is this: OSI has come a long way, and I am forever grateful to the talented and generous individuals who collectively invested decades to get us here.</p>

<p>Over the last seven years, OSI has: sustained its core mission, shaped policy around the globe, worked tirelessly to mitigate open washing, built an alliance of more than 125 organizations representing hundreds of thousands of people, provided a home for projects like <a href="https://clearlydefined.io/">ClearlyDefined</a>, and rolled out programs like <a href="https://opensource.com/article/17/9/floss-desktops-kids">FLOSS Desktops for Kids</a> and <a href="https://www.brandeis.edu/gps/future-students/learn-about-our-programs/open-source-technology-management.html">Open Source Technology Management</a> courses with Brandeis University.</p>

<p>We have seen incremental progress every year. OSI has expanded its programs and refined its operations. The trouble is, our operational capacity has not kept pace with the growing responsibility.</p>

<h2>What Comes Next</h2>

<p>Two years ago, the <a href="https://opensource.org/docs/board-annotated">OSI Board</a> recognized the need for another transformation, and made staffing the organization a priority. Pursuing that has required us to shave a great number of yaks! Along the way, we've identified several other key changes, all of which are reflected in our <a href="https://wiki.opensource.org/bin/Operations/Annual+Planning+Process/2020-Annual-Plan">Annual Initiatives</a> and the efforts our <a href="https://opensource.org/organization">Committees</a> have in flight.</p>

<p>In broad strokes, we have deep organizational development work to do (more on that later) as well as community engagement initiatives including:</p>

<ul>
	<li>Create more opportunities for people to make their voices heard and get involved with the process, by convening Working Groups and Advisory Boards to work in concert with our Committees.</li>
	<li>Develop a communications plan and capabilities in order to be responsive to community developments, as well as lead and facilitate emerging conversations.</li>
	<li>Invest in an updated Code of Conduct and moderation tools.</li>
	<li>Continue investing in documentation in service of transparency.</li>
	<li>Continue targeted recruitment in service of representation.</li>
	<li>Hire people to serve as Community and Communications Manager supporting this work.</li>
</ul>

<p>Suffice it to say, we have our work cut out for us!</p>

<h2>Well That Sounds Promising</h2>

<p>The good news is that OSI has never been in a better position than it is now. We have all the right players on all the right bases. We're more organized than ever. And an intense period in the spotlight has brought the work we need to do into sharp focus.</p>

<p>The bad news is that we only have 5 months operating budget in reserve, thus lack the funding to hire additional staff... And we're likely to suffer a 20-40% drop in revenue due to the pandemic and resulting economic downturn.</p>

<h2>Your Role In This</h2>

<p>This is where you come in. We need your voice to make sure we plot a path forward that meets your needs, and we need your support to fund the work ahead.</p>

<p>We'll be sharing about new opportunities to make your voice heard as this work unfolds, but you can always reach us on <a href="https://opensource.org/resources">social media</a>, <a href="https://opensource.org/resources">IRC</a>, and directly via our <a href="https://opensource.org/lists">mailing lists</a> or <a href="https://opensource.org/contact">contact form</a>. And the OSI Board, being community-elected, is chock full of people whose job it is to serve you--you can always reach out to us individually.</p>

<p>If you are not yet an <a href="https://opensource.org/join">Individual Member</a>, we ask you to become one, which will keep you apprised of our work and allow you to vote in our annual elections, or even nominate yourself.</p>

<p>If you lead a nonprofit organization or community that believes in open source, we encourage you to become an <a href="https://opensource.org/affiliates">Affiliate Member</a>, which helps us provide mutual support and allows you to nominate and vote in elections.</p>

<p>And if your company relies on open source to conduct business, we ask that you invest back into the community that makes your work possible by becoming a <a href="https://opensource.org/sponsors">Corporate Sponsor</a>.</p>

<p>Together, we'll make sure that open source continues to thrive.</p>

<p>With gratitude,</p>

<p>Josh Simmons<br />
President<br />
Open Source Initiative</p>
